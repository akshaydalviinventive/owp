<?php
if ( is_active_sidebar( 'sidebar-1' ) || is_active_sidebar( 'sidebar-2' )) {?>
        <div id="sidebar-widget">
			<div class="row">
				<?php if ( is_active_sidebar( 'sidebar-1' )) : ?>
					<div class="widget-area" role="complementary">
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					</div><!-- #secondary -->
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'sidebar-2' )) : ?>
					<div class="widget-area" role="complementary">
						<?php dynamic_sidebar( 'sidebar-2' ); ?>
					</div><!-- #secondary -->
				<?php endif; ?>
			</div>
        </div>
<?php } ?>