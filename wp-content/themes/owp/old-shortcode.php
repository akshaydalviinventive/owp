<?php
function download_login() {
ob_start();
$title = get_the_title( $ID );
$page = (string)$title;

if ( is_user_logged_in() ) {} else {?>
<div class="download">
		<div class="signin-drop-inner-page">
			<div class="block">Download</div>
				<div class="signin-drop-content-inner-page">
				<span>Sign in with :</span>
				<ul>
					<li onclick="ga('send','event','Login','Facebook','<?php echo $page ?>');"><?php echo do_shortcode( '[TheChamp-Login]' ); ?></li>
				</ul>	
				</div>
		</div>
</div>
<?php } 

$output = ob_get_clean();

return $output;
}
?>