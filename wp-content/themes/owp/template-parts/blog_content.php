<style>
.single-post-box .post-thumbnail {
    margin-bottom: 10px;
    padding-top: 15px;
}
.cust-post-meta{
	margin: 10px 0px;
}
</style>
<div class="col-md-12"><!--Left col-->

	<div class="right-thumb" id="post-<?php the_ID(); ?>" <?php post_class(); ?>><!--Post Thumbnail-->			
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
	</div><!-- .Post Thumbnail -->
	<div class="cust-post-meta">
	<?php
		$post_date = get_the_date( 'l F j, Y' ); 
		echo '<span style="font-style: italic;">'. $post_date. '</span>';
	?>
	</div>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-content">
		<?php
		if ( is_single() ) :
		the_content();
		else :
		the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'owp' ) );
		endif;
		?>
		</div><!-- .entry-content -->

	</article><!-- #post-## -->

</div><!-- .Left col -->