<?php 
$post_date = get_the_date( 'l F j, Y' );
?> 
<style>
.time-stamp-box{    
    background-color: #9e9e9e;
    position: unset;
    padding: 0 10px;
    font-size: 12px;
    font-weight: normal;
    color: white;
}
.home_content h2 {
    font-size: 15px;
    padding: 10px 15px;
    height: 80px;
    background: #f7f7f7;
    margin-top: 5px;
    font-weight: bold;
    margin-bottom: 0;
}
.blog-exe-box{
	font-size: 15px;
	padding: 10px 15px 0px;
	height: unset;
	font-weight: normal;
	height: 115px;
}
.home_content article{
    text-align: center;
    height: auto;
    padding-bottom: 15px;
}
.home_content .entry-header{
	padding: 0;
}
.blog-read-more a{
    background: #44c182;
    padding: 5px 10px;
    color: white;
	border: 1px solid #44c182;
}
.blog-read-more a:hover{
	background: white;
    padding: 5px 10px;
    color: #44c182;
	border: 1px solid #44c182;
	text-decoration: none;
}
</style>
<div class="col-xs-12 col-md-4">

	<div class="home_content"><!--box-->

			<article id="post-<?php the_ID(); ?>">

				<div class="post-thumbnail">
					<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				</div>
				
				<div class="time-stamp-box">Published on <?php echo /*get_the_author_meta('display_name', $author_id) "&nbsp;On&nbsp;".*/ $post_date; ?></div>
				
				<header class="entry-header">

					<?php
						if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
						else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

						if ( 'post' === get_post_type() ) : 

						endif; 
					?>

				</header><!-- .entry-header -->
				
			<div class="blog-exe-box"><?php the_excerpt(); ?></div>
			<div class="blog-read-more"><a href="<?php echo get_permalink(); ?>">Read More</a></div>	
			</article><!-- #post-## -->
		
	</div><!--.box-->

</div>