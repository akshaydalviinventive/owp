<div class="col-xs-12 col-md-3">
<div class="home_content"><!--box-->

	<article id="post-<?php the_ID(); ?>">

		<div class="post-thumbnail">
			<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		</div>
		
		<header class="entry-header">
			<?php
				if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
				else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

				if ( 'post' === get_post_type() ) : 
			
				endif; 
			?>
		</header><!-- .entry-header -->
		
	</article><!-- #post-## -->
	
</div><!--.box-->
</div>