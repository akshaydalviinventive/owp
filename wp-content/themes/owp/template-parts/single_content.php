<div class="col-md-12"><!--Left col-->

	<div class="right-thumb" id="post-<?php the_ID(); ?>" <?php post_class(); ?>><!--Post Thumbnail-->			
		<div class="post-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
	</div><!-- .Post Thumbnail -->
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
		<div class="entry-content">
		<?php
		if ( is_single() ) :
		the_content();
		else :
		the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'owp' ) );
		endif;
		?>
		</div><!-- .entry-content -->

	</article><!-- #post-## -->
	
	<!--Disclaimer code-->
	<div class="disclaimer-low">
	Disclaimer: By downloading this whitepaper from OnlineWhitepapers.com, you will automatically be subscribed to our weekly newsletter. If you do not wish to receive our weekly newsletter, please unsubscribe using the link available in the newsletter. Unsubscribing from our newsletter will not affect your ability to download future whitepapers. Thank you. ( View our email privacy policy <a href="https://www.onlinewhitepapers.com/privacy-policy/" target="_blank">here</a>. )
	</div>
	<!--Disclaimer cod end-->

</div><!-- .Left col -->