<?php
$id = get_the_ID();
$cats =  get_the_category($id);//$post->ID
foreach($cats as $ct){
$ct->name;
} 
//Fetch custom texenomy 
$vterms = get_the_terms( $post->ID , 'video' );
foreach ( $vterms as $vterm ) {
$vterm->name;
}
?>

<div class="col-xs-12 col-md-3">
<div class="home_content"><!--box-->

<!--Category Tags-->
<?php if( $ct->name == 'Information Technology'){?>
<div class="cat-box" style="background-color: #35aae1;"><?php echo $ct->name="IT"; ?></div>
<?php }elseif($ct->name == 'Human Resources'){?>
<div class="cat-box" style="background-color: #ca46ad;"><?php echo $ct->name="HR"; ?></div>
<?php }elseif($ct->name == 'Finance'){?>
<div class="cat-box" style="background-color: #5bb901;"><?php echo $ct->name="FINANCE"; ?></div>
<?php }elseif($ct->name == 'Marketing'){?>
<div class="cat-box" style="background-color: #f4516d;"><?php echo $ct->name="MARKETING"; ?></div>
<?php }else{?>
<div class="cat-box" style="background-color: black;"><?php echo $ct->name; ?></div>
<?php }?>
<!--End Category Tags-->

<!--Video Tags-->
<?php if( $vterm->name == 'whitepapers without videos'){}else{?>
<div class="cat-video-box" style="background-color: #f1552c;"><a href="<?php echo get_permalink(); ?>"><?php echo "Watch Video"; ?></a></div>
<?php }?>
<!--End Video Tags-->

	<article id="post-<?php the_ID(); ?>">

		<div class="post-thumbnail">
			<a href="<?php echo get_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		</div>
		
		<header class="entry-header">
			<?php
				if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
				else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

				if ( 'post' === get_post_type() ) : 
			
				endif; 
			?>
		</header><!-- .entry-header -->
		
	</article><!-- #post-## -->
	
</div><!--.box-->
</div>