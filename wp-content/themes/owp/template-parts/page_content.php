<div class="col-md-12">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	

		<div class="entry-content">
			<?php
				if ( is_single() ) :
				the_content();
				else :
				the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'owp' ) );
				endif;
			?>
		</div><!-- .entry-content -->

	</article><!-- #post-## -->

</div><!-- .Left col -->