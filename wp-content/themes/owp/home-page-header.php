<?php // ?>
<div class="jumbotron text-center">
	<div class="container">
	  <h1 class="display-3">OnlineWhitepapers is the Largest Digital Library of B2B Whitepapers<br> in Written, Video, and Interactive Formats</h1>
			<?php echo do_shortcode( '[searchandfilter id="5155"]' ); ?>
		 <!-- <div class="cust-h-tag">
			 <span>Also search for:</span>
				<ul>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=System">System</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=Streamline">Streamline</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=Cloud">Cloud</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=AI">AI</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=Financial%20Services">Financial Services</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=Planning">Planning</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?sfid=5155&_sf_s=HR%20Technology">HR Technology</a></li>
				</ul>
		 </div> -->
		 <!-- <div class="home-cust-sort">
			<ul>
				<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li> -->
				<!--<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>-->
				<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>-->
			<!-- </ul>
		</div> -->
	</div>
</div>

<!--Company logo-->
<div class="filter_bar">
	<div class="container" style="background: white;margin-top: -30px;">
		<div class="row">
			<div class="cust-sort-slider">
				<!-- <h2>Featuring whitepapers from these and other companies</h2> -->
					<?php echo do_shortcode('[hcs]'); ?>
					<!--<a href="https://owpstaging.wpengine.com/4255-2/"><button type="button" class="btn-green">View All</button></a>-->
			</div>
		</div>
	</div>
</div>
<!--.Company logo end>
<!--cmw banner-->
<!--<div class="filter_bar">
	<div class="container">
		<div class="row">
			<div class="cust-sort-form">
				<a href="https://www.onlinewhitepapers.com/content-marketing-world/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2018/08/CMW-OWP-Newsletter-Banner-728-89-1.jpg" class="img-responsive"></a>
			</div>
		</div>
	</div>
</div>-->
<!--end-->
<!--<div class="filter_bar sticky-top">
	<div class="container">
		<div class="row">
			<div class="cust-sort-form"><?php //echo do_shortcode('[cpf]'); ?></div>
			<div class="cust-sort-form"><?php //echo do_shortcode( '[searchandfilter fields="category,video,geolocation,post_date" types=",,,daterange" all_items_labels="Category,Video Type,Geolocations,," submit_label="Filter"]' ); ?></div>
		</div>
	</div>
</div>-->

<!-- resources by category -->
<div class="container">
	<div class="resource-title">
	FIND RESOURCES BY CATEGORY
    </div>	
	<div class="row m-55">
		<div class="col-xs-12 col-md-3 m-35">
			<div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		    <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
    </div>	
</div>	
<!-- end of category -->

<!-- resources by industry -->

<div class="container">
	<div class="resource-title">
	FIND RESOURCES BY INDUSTRY
    </div>	
	<div class="row m-55">
		<div class="col-xs-12 col-md-3 m-35">
			<div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		    <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
    </div>	
</div>	
<!-- end of industry -->

<!-- subscribe section -->

<div class="subscribe-bg">
  <div class="container-fluid">
	<div class="subscribe-parent">
		<div class="sub-image">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/subscribe-icon.png">
        </div>
		<div class="subscribe-t-p">
			<div class="sub-h">
			    Subscribe
            </div>	
			<div class="sub-s">
				Subscribe to get the most recent OWP
            </div>
			<div class="form-group has-envelope">
         		 <span class="fa fa-envelope form-control-feedback"></span>
   				 <input type="text" class="form-control" placeholder="">
  			</div>
			<div class="subscribe-btn">
				<button type="button" class="btn-subscribe">SUBSCRIBE ME!</button>
            </div>	  	
        </div>	
    </div>
  </div>	
</div>
<!-- end of subscribe section -->

<!-- resources by job title -->

<div class="container">
	<div class="resource-title">
	FIND RESOURCES BY JOB TITLE
    </div>	
	<div class="row m-55">
		<div class="col-xs-12 col-md-3 m-35">
			<div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		    <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
		<div class="col-xs-12 col-md-3 m-35">
		   <div class="cat-title">
				Cloud
			</div>
		</div>
    </div>	
</div>	
<!-- end of resouces by job title -->

<!-- More resources section -->
<div class="container">
	<div class="row m-55">
		<div class="col-md-12 col-xs-12">
		<div class="more-resource-head more-c">
			Looking for more resources by category?
		  <div class="click-btn">
			<button type="button" class="click">Click here!</button>
          </div>
		</div>
        </div>	
	</div>	
</div>	
<!-- end of more resources section -->

<!-- content syndication section -->

<div class="container">
<div class="row m-55 bg-w">
<div class="col-xs-12 col-md-3">
	<div class="content-syndication-head">
		LEARN MORE<br> ABOUT<br> CONTENT SYNDICATION
	</div>	
</div>
<div class="col-xs-12 col-md-3 m-35">
  <div class="content-parent">
	<div class="content-img">
		<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn1.png">
	</div>
	<div class="content-icon-p">
		<div class="content-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn-icon.png">
		</div>
		<div class="content-syn-t">
			Top 5 Benefits of Whitepaper Publishing for Marketers
		</div>
	</div>
	<div class="content-sub">
	   With the massive amount of content being thrown at us daily, it is mandatory that we
	   pick and choose which content we will pay attention to ...
	</div>
	<div class="social-icon-p">
		<div class="copy-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="share-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="play-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/play-icon.png">
		</div>
	</div>
  </div>	
</div>

<div class="col-xs-12 col-md-3 m-35">
  <div class="content-parent">
	<div class="content-img">
		<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn2.png">
	</div>
	<div class="content-icon-p">
		<div class="content-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn-icon.png">
		</div>
		<div class="content-syn-t">
			Is Content Syndication Right for You?
		</div>
	</div>
	<div class="content-sub">
	   With the massive amount of content being thrown at us daily, it 
	   is mandatory that we pick and choose which content we will pay attention to ...
	</div>
	<div class="social-icon-p">
		<div class="copy-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="share-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="play-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/play-icon.png">
		</div>
	</div>
  </div>	
</div>

<div class="col-xs-12 col-md-3 m-35">
  <div class="content-parent">
	<div class="content-img">
		<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn1.png">
	</div>
	<div class="content-icon-p">
		<div class="content-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/content-syn-icon.png">
		</div>
		<div class="content-syn-t">
			4 Proven Tips On How To Market Your Whitepapers Right
		</div>
	</div>
	<div class="content-sub">
	   With the massive amount of content being thrown at us daily, it is mandatory that we
	   pick and choose which content we will pay attention to ...
	</div>
	<div class="social-icon-p">
		<div class="copy-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="share-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/share-icon.png">
		</div>
		<div class="play-icon">
			<img src="http://localhost/owp/wp-content/uploads/2021/04/play-icon.png">
		</div>
	</div>
  </div>	
</div>

</div>
</div>