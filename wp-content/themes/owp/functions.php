<?php
//Set size
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
//Code end

// include css and scripts
function load_scripts(){
	wp_enqueue_style('owp', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.1.0', 'all');
	wp_enqueue_style('cookieconsent', get_template_directory_uri() . '/css/cookieconsent.min.css', array(), 'null', 'all');
	wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css', array(), 'null', 'all');
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '4.1.0', true);
	wp_enqueue_script('jquery-3.4.1', 'https://code.jquery.com/jquery-3.4.1.min.js', array('jquery'), '3.4.1', true);
}
add_action('wp_enqueue_scripts', 'load_scripts');
//Code end

//Hide admin bar from all users ecxept admin
add_action('after_setup_theme', 'remove_admin_bar'); 
function remove_admin_bar(){
	if (!current_user_can('administrator') && !is_admin() && !current_user_can( 'editor' )){
		show_admin_bar(false);
	}
}
//Code End

//Add title tag
add_theme_support( 'title-tag' );
//Code end


//Redirect to home
// Block Access to /wp-admin for subscribers.
function custom_blockusers_init() {
  if ( is_user_logged_in() && is_admin() && current_user_can( 'subscriber' ) ) {
    wp_redirect( home_url() );
    exit;
  }
}
add_action( 'init', 'custom_blockusers_init' );
//Code end

//Excerpt count
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

//Setup logo
function owp_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'owp_custom_logo_setup' );
//Code end

//Register menus
function register_owp_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' )
    )
  );
}
add_action( 'init', 'register_owp_menus' );
//Code end

//Footer widgets
function owp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar widget 1', 'owp' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar widget 2', 'owp' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Widget 1', 'owp' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Widget 2', 'owp' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Widget 3', 'owp' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	 register_sidebar( array(
		'name'          => esc_html__( 'Widget 4', 'owp' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	 register_sidebar( array(
		'name'          => esc_html__( 'widget 5', 'owp' ),
		'id'            => 'footer-5',
		'description'   => esc_html__( 'Add widgets here.', 'owp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'owp_widgets_init' );
//Code end

//Featured images to rss feed
function featuredtoRSS($content) {
global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '<div>' . get_the_post_thumbnail( $post->ID, 'medium', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
}
return $content;
}
add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');
//Code end

//Post thumbnail support
function owp_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'owp_post_thumbnails' );
//End post thumbnail support

//For including shortcode for social logins
include(WP_CONTENT_DIR . '/themes/owp/shortcode.php');
add_shortcode( 'dl', 'download_login' );
//Code end

//For custom post filter
include(WP_CONTENT_DIR . '/themes/owp/sidebar-login.php');
add_shortcode( 'sidebar-login', 'sidebar_login' );
//Code end

//Search result
/*function bac_filter_wp_search($query) { 
    //Filter Search only in the front-end and not in the Admin.
    if (!$query->is_admin && $query->is_search) {
    //Search in Posts only. Excludes Pages, Attachments and CPTs.
    $query->set('post_type', 'post');
    }
    return $query;
}
add_filter('pre_get_posts','bac_filter_wp_search');*/
//End code

//For custom post filter
include(WP_CONTENT_DIR . '/themes/owp/post-filter/filterform.php');
add_shortcode( 'cpf', 'cpf_action' );
//Code end

/*Popular posts*/
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }
    else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
/*Code end*/

//End of code
//Blog category template code
function add_posttype_slug_template( $single_template ){
    if( has_category( 'blog' ) ){
        $single_template = include(WP_CONTENT_DIR . '/themes/owp/single-blog-template.php');
    }
    return $single_template;
}
add_filter( 'single_template', 'add_posttype_slug_template' );
//Code end

//Linkedin redirect
add_action('pkli_linkedin_authenticated', 'c_l_redirect');

function c_l_redirect(){
	echo "<script type='text/javascript'>window.location='https://www.onlinewhitepapers.com/lin-action/'</script>";
	exit();
}
//Code end

/*redirect search
function my_search_template_redirect() {
  if ( is_search() ) {
    include( get_stylesheet_directory() . '/search.php' );
    exit();
  }
}
add_action( 'template_redirect', 'my_search_template_redirect', 1);
//Code end
*/
/*REST API for featured image*/
add_action('rest_api_init', 'register_rest_images' );
function register_rest_images(){
    register_rest_field( array('post'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_rest_featured_image( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
        return $img[0];
    }
    return false;
}
/*Code End*/

/*REST API for category name*/
add_action( 'rest_api_init', 'register_categories_names_field' );
function register_categories_names_field() {

    register_rest_field( array('post'),
        'category_name',
        array(
            'get_callback'    => 'categories_names',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function categories_names( $object, $field_name, $request ) {
    $formatted_categories = array();
    $categories = get_the_category( $object['id'] );
    foreach ($categories as $category) {
        $formatted_categories = $category->name;
    }
    return $formatted_categories;
}
/*Code end*/

/*REST API for author name*/
add_action('rest_api_init', 'register_author_meta_rest_field');
function register_author_meta_rest_field() {

    register_rest_field('post', 'author_meta', array(
        'get_callback'    => 'get_author_meta',
        'update_callback' => null,
        'schema'          => null,
    ));

}

function get_author_meta($object, $field_name, $request) {

    $user_data = get_userdata($object['author']); // get user data from author ID.

    $array_data = (array)($user_data->data); // object to array conversion.

    $array_data['first_name'] = get_user_meta($object['author'], 'first_name', true);
    $array_data['last_name']  = get_user_meta($object['author'], 'last_name', true);

    // prevent user enumeration.
    unset($array_data['user_login']);
    unset($array_data['user_pass']);
    unset($array_data['user_activation_key']);

    return array_filter($array_data);
}
/*Code end*/

//CS slider plugin
include(WP_CONTENT_DIR . '/themes/owp/cslider/company-slider.php');
add_shortcode( 'hcs', 'home_company_slider' );
//Code End

/*REST API for wp url*/
add_action('rest_api_init', 'register_wp_url' );
function register_wp_url(){
    register_rest_field( array('post'),
        'wp_url',
        array(
            'get_callback'    => 'get_rest_wp_url',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_rest_wp_url( $object, $field_name, $request ) {
    
	return $test = "https://www.onlinewhitepapers.com/wp-content/uploads/download-manager-files/";
 
    return false;
}
/*Code End*/

/* Custom Post Type Briteverify */
function create_posttype() {
register_post_type( 'briteverify',
// CPT Options
array(
  'labels' => array(
   'name' => __( 'briteverify' ),
   'singular_name' => __( 'briteverify' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'briteverify'),
 )
);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
/* Custom Post Type End */


/*Custom Post type start briteverify*/
 function cw_post_type_briteverify() {
$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'custom-fields', // custom fields
'comments', // post comments
'revisions', // post revisions
'post-formats', // post formats
);
$labels = array(
'name' => _x('Not Briteverify Email Address', 'plural'),
'singular_name' => _x('briteverify', 'singular'),
'menu_name' => _x('Not Briteverify Email', 'admin menu'),
'name_admin_bar' => _x('briteverify', 'admin bar'),
'add_new' => _x('Add New', 'add briteverify'),
'add_new_item' => __('Add New Briteverify Email'),
'new_item' => __('New Briteverify'),
'edit_item' => __('Edit Briteverify'),
'view_item' => __('View Briteverify'),
'all_items' => __('All Briteverify'),
'search_items' => __('Search Briteverify'),
'not_found' => __('No briteverify email address found.'),
);
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'briteverify'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('briteverify', $args);
}
add_action('init', 'cw_post_type_briteverify'); 
/*Custom Post type end briteverify*/


?>