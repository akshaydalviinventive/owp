<?php get_header(); ?>
<header class="jumbotron text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="padding: 0px;">
					<?php
					if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
					else :
					the_title( '<h1 class="entry-title">', '</h1>' );
					endif;
					?>		
					<?php echo do_shortcode( '[searchandfilter fields="search" submit_label="&#128269;"]' ); ?>
					<div class="home-cust-sort">
						<ul>
							<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li>
							<!--<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>-->
							<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
</header>
	<div class="single-post-box">
		<div class="container">
			<div class="row">
					<?php
					while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/page_content', get_post_format() );
					endwhile; // End of the loop.
					?>
			</div>
		</div>	
	</div>
<?php get_footer(); ?>