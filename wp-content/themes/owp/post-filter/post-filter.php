<?php
$abs_path= __FILE__;
$get_path=explode('wp-content',$abs_path);
$path=$get_path[0].'wp-load.php';
include($path);

	$a = $_POST['categoryfilter'];
	$b = $_POST['tagfilter'];
	
	if(isset($a) && $b=="Content Type"){

		//echo "You Search for " .$a. "category<br>";
		
		$args['tax_query'] = array(
						array(
							'taxonomy' => 'category',
							'field' => 'id',
							'terms' => $_POST['categoryfilter']
						)
					);			

	}elseif(isset($b) && $a=="Categories" && $c == ""){
	
		//echo "You Search for " .$b. "Post type<br>";
		
		$args['tax_query'] = array(
						array(
							'taxonomy' => 'post_tag',
							'field' => 'id',
							'terms' => $_POST['tagfilter']
						)
					);	

	}elseif(isset($a) && isset($b)){
	
		//echo "You select both field<br>";

		$args = array(
			'post_type' => 'post',
			'tax_query' => array(
				'relation' => 'AND',
					array(
						'taxonomy' => 'category',
						'field'    => 'id',
						'terms' => $_POST['categoryfilter'],
					),
					array(
						'taxonomy' => 'post_tag',
						'field' => 'id',
						'terms' => $_POST['tagfilter'],
					),
				),
			);
			
	}elseif($a=="Categories" && $b=="Content Type"){
		
		echo "Select any";
		
	}else{}

	$query = new WP_Query($args);

get_header();
?>

<?php get_template_part( 'archive-page-header' ); ?>

<div class="container">

	<div class="row">
		<?php if ( $query->have_posts() ) : ?>
		
		<?php

		while ( $query->have_posts() ) : $query->the_post();

			get_template_part( 'template-parts/post_filter_content' );

		endwhile;
		
		else :

			get_template_part( 'template-parts/post_filter_content', 'none' );

		endif; 
	
		wp_reset_postdata(); 
	
		?>

	</div>

</div>

<?php get_footer(); ?>