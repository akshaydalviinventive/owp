<?php 
function cpf_action(){ 
ob_start();
?>
		<form action="<?php echo get_stylesheet_directory_uri(); ?>/post-filter/post-filter.php" method="POST" id="filter">
		<span style="font-weight: bold;margin: 5px 7px;color: #555;">Filter by:</span>
			<!--<input type="text" id="search" name="s" placeholder="Search..." />-->
			<?php
				if( $terms = get_terms( 'category', 'orderby=name' ) ) : // to make it simple I use default categories
					echo '<div class="selectdiv"><select name="categoryfilter"><option>Categories</option>';
					foreach ( $terms as $term ) :
						echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
					endforeach;
					echo '</select></div>';
				endif;
			?>
			<?php
				if( $terms = get_terms( 'post_tag', 'orderby=name' ) ) : // to make it simple I use default categories
					echo '<div class="selectdiv"><select name="tagfilter"><option>Content Type</option>';
					foreach ( $terms as $term ) :
						echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; // ID of the category as the value of an option
					endforeach;
					echo '</select></div>';
				endif;
			?>
			
			<input type="submit" class="btn btn-light" value="Apply Filter">
		</form>
<?php 
	$output = ob_get_clean();
	return $output;
}
?>