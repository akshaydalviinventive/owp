<div class="sb-wp">
<div class="container">
<h2>Get Your Whitepaper in Front of Millions of Readers and Buyers.</h2>
<p style="font-size: 24px;font-weight: lighter;">Publish your whitepaper with us. We have flexible plans to suit every company size and budget.<br/>Choose the one that works for you and submit today.</p>
<a href="http://submit.onlinewhitepapers.com"><button type="button" class="btn-green">LEARN MORE</button></a>
</div>
</div>
<?php get_template_part( 'footer-widget-1' ); ?>
<?php get_template_part( 'footer-widget-2' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
            <div class="site-info">
                &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">OnlineWhitepapers.com</a> - Powered by <a href="https://www.bython.com/" target="_blank">Bython</a> | <a href="https://www.onlinewhitepapers.com/privacy-policy">Privacy Policy</a> | <a href="https://www.onlinewhitepapers.com/terms-of-use/">Terms Of Use</a> | <a href="https://www.onlinewhitepapers.com/disclaimer/">Disclaimer</a>'; ?>
               
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
	
	<!--<script src="<?php //echo get_stylesheet_directory_uri(); ?>/js/jquery-3.3.1.slim.min.js"></script>
	<script src="<?php //echo get_stylesheet_directory_uri(); ?>/js/popper.min.js"></script>
	<script src="<?php //echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>-->
	
<!--<script type="text/javascript">
document.body.innerHTML = document.body.innerHTML.replace(/All Categories/g, 'Categories');
document.body.innerHTML = document.body.innerHTML.replace(/All Tags/g, 'Content Type');
</script>-->

<!--make sticky download button code-->
<script>
(function($){
$( ".cust-img-strip" ).addClass( "sticky-top sticky-top-2" );
})(jQuery);
</script>
<!--code end-->

<!--Hide 2nd div-->
<script>
(function($){
var width = $(window).width();
    if (width <= 1199) {
      $('.dl_form').not(':first').hide();
    } else {}
})(jQuery);
</script>
<!--code end-->

<!--drift code by Daniella-->
<script>
drift.api.startInteraction({ interactionId: 96346 });
</script>
<!--code end-->

<script type="text/javascript">
document.body.innerHTML = document.body.innerHTML.replace(/Older posts/g, 'Previous');
document.body.innerHTML = document.body.innerHTML.replace(/Newer posts/g, 'Next');
document.body.innerHTML = document.body.innerHTML.replace(/All Tags/g, 'Content Type');
document.body.innerHTML = document.body.innerHTML.replace(/Download limit exceeded!/g, '<div class="cust_msg">You have exceeded your download limit!<br>Email us at: dw@bython.com if you require additional downloads.</div>');
</script>


<!--<script src="<?php //echo get_stylesheet_directory_uri(); ?>/js/cookieconsent.min.js"></script>
<script>
(function($){
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000",
      "text": "#ffffff"
    },
    "button": {
      "background": "#f1d600"
    }
  },
  "theme": "edgeless",
  "content": {
    "link": "Privacy Policy",
    "href": "https://www.onlinewhitepapers.com/privacy-policy"
  }
})});
})(jQuery);
</script>-->

<?php 
$title = get_the_title( $ID );
$page = (string)$title; 
?>
<!--Ga code-->
<script>
(function($){
var links = document.getElementsByClassName("wpdm-download-link [btnclass]");
for(var i = 0, len = links.length; i < len; i++) {
var defaultonclick = links[i].getAttribute('onclick');
var d = defaultonclick.toString();
var g = "ga('send','event','Download','WhitePaper','<?php echo $page ?>');";
links[i].setAttribute('onclick', g.concat(d)); // for FF
}
})(jQuery);
</script>


<!--Event tracking code by Varsha-->
<?php if(is_single( '5541' )){ ?>
<!--Ga code-->
<script>
(function($){
var links = document.getElementsByClassName("wpdm-download-link [btnclass]");
for(var i = 0, len = links.length; i < len; i++) {
var defaultonclick = links[i].getAttribute('onclick');
var d = defaultonclick.toString();
var g = "return gtag_report_conversion('https://www.onlinewhitepapers.com/download/erp-vs-plm-which-one-is-best/?wpdmdl=5540');";
links[i].setAttribute('onclick', g.concat(d)); // for FF
}
})(jQuery);
</script>
<!--End-->
<?php }else{ ?>
<!--Ga for youtube code-->
<script>
(function($){
var b = document.querySelector("iframe");
b.setAttribute("onclick", "ga('send','event','Video','Play','<?php echo $page ?>');");	
})(jQuery);
</script>
<!--End-->
<?php } ?>
<!--Footer Code-->

<!--track conversions from campaigns-->
<script type="text/javascript">
_linkedin_data_partner_id = "208826";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=208826&fmt=gif" />
</noscript>
<!--End of code for track conversions from campaigns-->

<!--Remove link from pdf title-->
<script type="text/javascript">
$("div.media-body").find("a").each(function(){
    var linkText = $(this).text();
    $(this).before(linkText);
    $(this).remove();
});
</script>
<!--Code end-->

<!--Dashboard code-->

<?php
$id = get_the_ID();
$title = get_the_title();
//echo "<h1>Page Id:-". $id ."</h1>";
//echo "<h1>Page Title:-". $title ."</h1>";
?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/Aneesh-data/JS/analytic.js"></script>
<script>
jQuery(function() {
    var wpid='<?php echo "1"; ?>';
    var wptitle='<?php echo "2"; ?>';
    var pid='<?php echo $id; ?>';
    var pagetitle='<?php echo $title; ?>';
   
    owppageview(wpid,wptitle,pid,pagetitle);   
   
});

jQuery(window).bind('beforeunload', function() {
    var data = new Date($.now()); // without jquery remove this $.now()
  
    var d = new Date,
    dformat = [d.getFullYear() ,d.getMonth()+1,
               d.getDate()
               ].join('-')+' '+
              [d.getHours(),
               d.getMinutes(),
               d.getSeconds()].join(':');
              
    //var pageUrl=window.location.href;   

    var protocol = location.protocol;
    var slashes = protocol.concat("//");
    var host = slashes.concat(window.location.hostname);   
              
    $.ajax({async: false, // Necessary, because the closing code has
                          // to be suspended until the ajax succeeds
        url: '<?php echo get_stylesheet_directory_uri(); ?>/Aneesh-data/storeupdate.php',
        type: "post",
       // data: {time: dformat,page:pageUrl},
        data: {time: dformat},
        success: function(text) {
            // This is executed when the response has been received
            // text is response data
            //alert(text);
            //document.getElementById('startTime').innerHTML=text;
           
        }
    });
   
});  
</script>
<!--End of Dashboard code-->

<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script><!--MacAfee-->

<!--Linkedin Tag for TF OWP By UJ 05-09-2018-->
<script type="text/javascript">

_linkedin_partner_id = "211091";

window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];

window._linkedin_data_partner_ids.push(_linkedin_partner_id);

</script><script type="text/javascript">

(function(){var s = document.getElementsByTagName("script")[0];

var b = document.createElement("script");

b.type = "text/javascript";b.async = true;

b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";

s.parentNode.insertBefore(b, s);})();

</script>

<noscript>

<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=211091&fmt=gif" />

</noscript>
<!--Code end-->
<?php wp_footer(); ?>
  </body>
</html>