<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-T6XKHHG');</script>
<!-- End Google Tag Manager -->

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--zoho page sense-->
<script src="https://cdn.pagesense.io/js/bython/fbff8b93bd1842718c0214c8e2f24f16.js"></script> 
<!--zoho page sense end-->

<!--Title tag-->
<title><?php wp_title(' '); ?><?php if(wp_title(' ', false)) { echo ' &raquo; '; } ?><?php bloginfo('name'); ?></title>
<!--Title tag end-->

<!--Stylesheets
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
<link rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri(); ?>/style.css" />
<link rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri(); ?>/css/cookieconsent.min.css" />
End Of Style Sheets-->

<!--Analytics code-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-106957905-1', 'auto');
  ga('require', 'GTM-NMRXDXR');
  ga('send', 'pageview');
</script>
<!--End Analytics code-->

<!--Code by Varsha 30.10-->
<!-- Global site tag (gtag.js) - Google Ads: 784207771 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-784207771"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-784207771');
</script>


<?php if(is_single( '5541' )){ ?>
<!-- Event snippet for ERPvsPLM Download conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-784207771/ZdAbCJXRv4sBEJuf-PUC',
      'event_callback': callback
  });
  return false;
}
</script>
<!--End-->
<?php }else{} ?>
<!--Header Code-->

<!--GA code by UJ-->
<!-- Global site tag (gtag.js) - Google Ads: 798181077 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-798181077"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-798181077'); </script>
<!-- Event snippet for Download conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> <script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-798181077/EMNoCJb3gIkBENWNzfwC', 'transaction_id': '', 'event_callback': callback }); return false; } </script>
<!--Code end-->

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> onload="document.refresh();">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6XKHHG" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div class="header-part">
	
	<div class="row">
		<div class="mobile-app-banner">
			<span>Download the OWP App <a href="https://www.onlinewhitepapers.com/mobile-app/" target="_blank">here</a>!</span>
		</div>
	</div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<div class="cust-navbar-brand" href="#" style="margin-top: 10px;">
						<?php if ( function_exists( 'the_custom_logo' ) ) {the_custom_logo();} ?>
					</div>
				</div>
				<div class="col-md-10">
					<nav class="navbar navbar-inverse" role="navigation">
								<?php wp_nav_menu( array( 
								'theme_location' => 'main-menu', 
								'container_class' => 'custom-menu-class'
								) ); ?>
					</nav>
					<!--Menu filter module-->				
					<div class="aside-filter-menu">
					<?php //echo do_shortcode( '[searchandfilter fields="search,category,video,post_date" types=",checkbox,checkbox,daterange" headings="Enter a search term,By Category,By Type,By Date"]' ); ?>
					<?php echo do_shortcode( '[searchandfilter id="5156"]' ); ?>
					</div>
					<script>
					(function($){	
					jQuery(document).ready(function(){
					jQuery('#menu-item-4420 a').on('click', function(event) {        
					jQuery('.aside-filter-menu').fadeToggle(500);
					});
					});	
					})(jQuery);
					</script>
				<?php
				//Call plugin shortcode
				if ( is_user_logged_in() ) {
				if (is_home()){
				?>	
					<div class="loginout"><a href="<?php echo wp_logout_url( home_url() ); ?>">Sign out</a></div>
				<?php 
				}else{
				?>
				<div class="loginout"><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Sign out</a></div> 
				<?php } } else{ ?>
					<!--<div class="signin-drop">
						<div class="block">Sign In</div>
							<div class="signin-drop-content">
							<ul>
								<li onclick="ga('send','event','Login','Facebook','Header');"><?php //echo do_shortcode( '[TheChamp-Login]' ); ?></li>
							</ul>	
							</div>
					</div>-->
				<?php } ?>
				<!--For mobile-->
					<div class="advsrch">More</div>
					<div class="aside-filter-menu-mobile">
					<?php //echo do_shortcode( '[searchandfilter fields="search,category,video,post_date" types=",checkbox,checkbox,daterange" headings="Enter a search term,By Category,By Type,By Date"]' ); ?>
					<?php echo do_shortcode( '[searchandfilter id="5156"]' ); ?>
					</div>
					<script>
					(function($){	
					jQuery(document).ready(function(){
					jQuery('.advsrch').on('click', function(event) {        
					jQuery('.aside-filter-menu-mobile').fadeToggle(500);
					});
					});	
					})(jQuery);
					</script>
				<!--Code end-->
				</div>
			</div><!--row end-->
		</div><!--container end-->
	</div><!--header end-->