<?php 
/*
Template name: pCloudy
*/
get_header(); 
$args = array(
	'post_type' => 'post',
	'tax_query' => array(
		array(
			'taxonomy' => 'company',
			'field'    => 'slug',
			'terms'    => 'pcloudy',
		),
	),
);
$query = new WP_Query( $args );
?>
<style>
.company-template-logo img{border-radius: 6px;border: 1px solid #edf0ef;text-align: center;background: white;height: auto;width: 135px;}
.company-template-logo img:hover{border: none;box-shadow: 0px 0px 65px 90px #ffffff;}
.cust-entry-header{padding-top: 25px;padding-bottom: 25px;}
</style><!--End Stylesheet-->
<header class="cust-entry-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="padding: 0px;">
					<div class="company-template-logo">
						<img src="https://www.onlinewhitepapers.com/wp-content/uploads/2019/05/pCloudy-Logo-255x255.png" alt="" width="255" height="255" class="alignnone size-full wp-image-8055" />
					</div>
				</div>
			</div>
		</div>			
</header><!--End Headerpart-->
<div class="container">
	<div class="row">
	<?php 
	if ( $query->have_posts() ) : 
	while ( $query->have_posts() ) : $query->the_post();
	get_template_part( 'template-parts/home_content', get_post_format() );
	endwhile;	
	else :
	echo 'no data found';
	endif; 
	wp_reset_postdata();
	?>
	</div>
</div>
<?php get_footer(); ?>