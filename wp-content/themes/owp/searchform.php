<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<input class="form-control border-secondary py-2" type="search" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search:', 'label', 'owp' ); ?>" placeholder="Search now:">
		<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="submit">
				<i class="fa fa-search"></i>
			</button>
		</div>
	</div>
</form>



