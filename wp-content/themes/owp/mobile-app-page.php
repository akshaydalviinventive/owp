<?php 
/*
Template name: mobile app
*/
get_header(); ?>
<style>
h3{font-size: 14px;}
.jumbotron {display: none;}
</style>
	<!--<header class="cust-entry-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12" style="padding: 0px;">
						<h1>Game On! Content Marketing World (CMW) 2018!</h1>		
					</div>
				</div>
			</div>			
	</header>-->
	<div class="single-post-box">
		<div class="container">
			<div class="row">
			<?php
				while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/page_content', get_post_format() );
				endwhile; // End of the loop.
			?>
			</div>
			<?php //echo do_shortcode('[cmw-feed]'); ?>
		</div>	
	</div>
<?php get_footer(); ?>