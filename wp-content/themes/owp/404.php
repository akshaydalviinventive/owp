<?php get_header(); ?>
<style>
.jumbotron {margin-bottom: 0;}
</style>
<div class="jumbotron text-center">
	<div class="container">
	  <div align="center"><h1>404!<h1><h2>It looks like nothing was found at this location.<br>Maybe try a search?</h2></div>
		<?php //echo do_shortcode( '[searchandfilter fields="search" submit_label="&#128269;"]' ); ?>
		<?php echo do_shortcode( '[searchandfilter id="5155"]' ); ?>
		 <div class="cust-h-tag">
			 <span>Also search for:</span>
				<ul>
					<li><a href="https://www.onlinewhitepapers.com/?s=system">System</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=streamline">Streamline</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=cloud">Cloud</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=AI">AI</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=financial%20services">Financial Services</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=Planning">Planning</a></li>
					<li><a href="https://www.onlinewhitepapers.com/?s=hr%20technology">HR Technology</a></li>
				</ul>
		 </div>
		 <div class="home-cust-sort">
			<ul>
				<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li>
				<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>
				<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>-->
			</ul>
		</div>
	</div>
</div>
<?php get_footer(); ?>