<?php get_header(); ?>
<div class="single-post-main">
	<header class="cust-entry-header">
			<div class="container">
				<div class="row">
					<div class="col-md-12" style="padding: 0px;">
						<?php
						if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
						else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;
						?>	
					</div>
				</div>
			</div>			
	</header>
	<div class="single-post-box">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<?php
					while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/single_content', get_post_format() );
					endwhile; // End of the loop.
					?>
				</div>
				<div class="col-md-3 owp-sidebar">	
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>	
	</div>
</div>	
<?php get_footer(); ?>