<?php 
/*
Template Name: Interactive Page
*/
get_header(); ?>
<style>
.jumbotron {
	margin-bottom: 0;
	border-radius: 0;
}
.container-fluid{
	padding-right: 0;
	padding-left: 0;
}
.row{
	margin-right: -15px;
	margin-left: -15px;
}
.single-post-box {
	margin-bottom: auto;
}
</style>
<header class="jumbotron text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="padding: 0px;">
					<?php
					if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
					else :
					the_title( '<h1 class="entry-title">', '</h1>' );
					endif;
					?>
				</div>
			</div>
		</div>
</header>
	<div class="single-post-box">
		<div class="container-fluid">
			<div class="row">
					<?php
					while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/page_content', get_post_format() );
					endwhile; // End of the loop.
					?>
			</div>
		</div>	
	</div>
<?php get_footer(); ?>