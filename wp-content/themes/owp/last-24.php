<?php 
/*
Template name: Last published posts in 24 hr
*/
//get_header();

	$args = array(
		'post_type' => 'post',
		'date_query' => array(
			array(
			'after' => '24 hours ago'
			)
		)
	);
$my_query = new wp_query( $args );
?>
<div class="container">
	<?php
	if( $my_query->have_posts() ) {
		echo '<h3>Posted whitepapers in last 24 hrs</h3>';
		while( $my_query->have_posts() ) {
			$my_query->the_post();
	?>
			<div class="relatedthumb"><a href="<? the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a></div>
			<h3><a href="<? the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
<?php
		}
	}
?>	
</div>	
<?php 
wp_reset_query();
//get_footer();
?>

