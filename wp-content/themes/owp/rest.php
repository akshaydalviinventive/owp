<?php 
/*
Template name: REST
*/
?>
<?php
echo $user_name = $_GET['user_name'];
echo $user_first_name = $_GET['user_f_name'];
echo $user_last_name = $_GET['user_l_name'];
echo $user_mailId = $_GET['mail_id']; 
echo $social_media_name = $_GET['sm_name'];
echo $profile_pic_url = $_GET['profile_pic_url'];
echo $device_os = $_GET['device_os'];

echo $bio = "This user is come from ".$social_media_name." using ".$device_os." device"; 

if ( email_exists( $user_mailId ) ) {
	$user = get_user_by( 'email', $user_mailId );
	$userId = $user->ID;
}

$user_id = wp_insert_user( array(
'user_login'  =>  $user_name,
'user_email' =>  $user_mailId,
'user_pass'   =>  NULL,  // When creating an user, `user_pass` is expected.
'first_name' => $user_first_name,
'last_name' => $user_last_name,
'description' => $bio
)) ;	

if ( ! is_wp_error( $user_id ) ) {
	$return = array('message' => "New user added");
	wp_send_json_success( $return );
}else{
	$return = array('message' => "User already exist");
    wp_send_json_error($return);
}
?>