<?php
function download_login() {
ob_start();
?>
<style>
.dl_form{
    float: left;
}
.dl_form .cform{
	float: left;
	padding: 9px;
	text-align: center;
	background: #44c182;
	border-radius: 5px;
}
.dl_form span{
	float: left;
    margin-right: 0;
    margin-left: 0px;
    font-size: 16px;
    font-weight: bold;
}
.dl_form .lform{
	float: left;
	color: black;
}
.dl_form .lform li{
	list-style: none;
    float: left;
}
.dl_form [type=submit]{
	border: 1px solid #44c182;
    background: #44c182;
    color: white;
    font-size: 16px;
    font-weight: bold;
    margin-top: 0px;
	cursor: pointer;
}
.sso-error {
    width: auto;
    position: absolute;
    border: 1px solid #008b8b;
    padding: 5px;
    background: #1f7f7f;
    color: #ffffff;
    top: 65px;
    font-size: 14px;
    text-align: center;
    margin-right: -2px;
}
</style>
<?php
if (isset($ID)){
$title = get_the_title( $ID );
}

$page = (string)$title;
//echo get_permalink();
if ( is_user_logged_in() ) {} else {
 //Code for activating the account
	if(isset($_GET['act'])){
		$data = unserialize(base64_decode($_GET['act']));
		$code = get_user_meta($data['id'], 'activation_code', true);
		$isActivated = get_user_meta($data['id'], 'is_activated', true);
		if( $isActivated ) {                                                
			echo 'This account has already been activated. Please log in with your registered email id'; // generates an error message if the account was already active
		}else{
			if($code == $data['code']){ // checks whether the decoded code given is the same as the one in the data base
			update_user_meta($data['id'], 'is_activated', 1); // updates the database upon successful activation
			$user_id = $data['id']; 
			$user = get_user_by( 'id', $user_id ); 

			if( $user ) {
				wp_set_current_user( $user_id, $user->user_login );
				wp_set_auth_cookie( $user_id );
				do_action( 'wp_login', $user->user_login, $user );
			}
				wp_redirect( get_permalink() ); exit;
			} else {
				echo '<div class="sso-error"><strong>Error:</strong> Account activation failed. Please try again in a few minutes or <a href="/sso/?u='.$userdata->ID.'">resend the activation email</a>.<br />Please note that any activation links previously sent lose their validity as soon as a new activation email gets sent.<br />If the verification fails repeatedly, please contact our administrator.</div>';
			}
		}
	}
	if(isset($_GET['u'])){// If resending confirmation mail
		my_user_register($_GET['u']);
		echo '<div class="sso-error">Your activation email has been resent. Please check your email and your spam folder.</div>';
	}

//Code for checking if submit button is clicked
if(isset($_POST["submit"])){
	
	//Get the submitted mail id
	$email_address = $_POST['user_email'];
	//echo $email_address;
	//Checking validation
	if(preg_match('/^([\w.-]+)@(\[(\d{1,3}\.){3}|(?!hotmail|gmail|googlemail|yahoo|gmx|ymail|outlook|bluewin|protonmail|inbox|icloud|mail|aol|yandex|abc|xyz|123|rediff|tutanota|minutemail|gmxmail|elude|test|t\-online|web\.|online\.|aol\.|live\.)(([a-zA-Z\d-]+\.)+))([a-zA-Z]{2,4}|\d{1,3})(\]?)$/i',$email_address)){
	//echo "Yeah, you have an corporate email address!";
	
	//Check if email exists
	if ( email_exists( $email_address ) ) {
	//Login process
		$user_mail = get_user_by( 'email', $email_address );
		$user_id = $user_mail->ID;
		$key = 'is_activated';
		$single = true;
		$user_last = get_user_meta( $user_id, $key, $single ); 
		//echo '<p>The '. $key . ' value for user id ' . $user_id . ' is: ' . $user_last . '</p>'; 
		$has_activation_status = get_user_meta($user_id, 'is_activated', false);
		if ($has_activation_status) {	// checks if this is an older account without activation status; skips the rest of the function if it is
			$isActivated = get_user_meta($user_id, 'is_activated', true);
			if ( !$isActivated ) { // resends the activation mail if the account is not activated
				echo '<div class="sso-error"> To Sign In, please click on the verification link sent to ' . $email_address . '!</div>';
			}else{
				echo $email_address;
				$user_mail = get_user_by( 'email', $email_address );
				echo $user_id = $user_mail->ID;
				if( $user_mail ) {
					wp_set_current_user( $user_id, $user->user_login );
					wp_set_auth_cookie( $user_id );
					do_action( 'wp_login', $user->user_login, $user );
					wp_redirect( get_permalink() ); exit;
				}else{
					echo '<div class="sso-error">Failed to Sign In!</div>';
				}
			}
		}
	}else{//Registartion process
		$password = wp_generate_password( 7, true, true );
		$user_id = wp_create_user( $email_address, $password, $email_address ); // Generate the password and create the user

		// Set the nickname
		wp_update_user(
		array(
			'ID'          =>    $user_id,
			'nickname'    =>    $email_address
			)
		);

		// Set the role
		$user = new WP_User( $user_id );
		$user->set_role( 'subscriber' );

		$user_info = get_userdata($user_id); // get user data
		
		$code = md5(time()); // create md5 code to verify later
		
		$string = array('id'=>$user_id, 'code'=>$code); // make it into a code to send it to user via email
		
		update_user_meta($user_id, 'is_activated', 0); // create the activation code and activation status
		update_user_meta($user_id, 'activation_code', $code);
		
		//echo get_permalink();
		
		$url = get_permalink(). '?act=' .base64_encode( serialize($string)); // create the url
		
		$html = 'Please click on the link below to activate your account:- '.$url.''; // basically we will edit here to make this nicer
		
		//echo $user_info->user_email; //checking mail id
		
		$mail_result = wp_mail( $user_info->user_email, __('Email Subject','text-domain') , $html); // send an email out to user
		
		if (isset($mail_result)){ //check mail is send or not
			echo '<div class="sso-error"> Please check your mail ' . $email_address .' for activation link. Thanks!</div>';
		}else{
			echo '<div class="sso-error">Error sending activation link</div>';	
		}
	}//Registartion process complete
}else{
	echo '<div class="sso-error">Oops, your email address is not valid or is not corporate email address!</div>';
	}
}else{//Code for checking if submit button is clicked	
	//echo "Enter valid coorporate mail id";
}
?>
<div class="dl_form">

	<div class="cform">
		<form  action="<?php the_permalink(); ?>" method="post">
				<input type="email" name="user_email" id="user_email" placeholder="Corporate Email ID*" value="" required>
				<input type="submit" name="submit" value="Sign in"/>
		</form>
	</div>

<!--<span style="margin-right: 10px;margin-left: 10px;padding: 10px 6px;">OR</span>

	<div class="lform">
	<li onclick="ga('send','event','Login','LinkedIn','<?php //echo $page ?>');"><?php //echo do_shortcode( "[wpli_login_link]" ); ?></li>	
	</div>-->	

</div>
<?php } 

$output = ob_get_clean();

return $output;
}
?>