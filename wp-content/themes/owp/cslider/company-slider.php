<?php function home_company_slider(){ ?>
<style>
/* Make the image fully responsive */
.carousel-inner img {
width: auto;
height: auto;
margin-right: 50px;
}
.carousel-indicators li{
border: 1px solid;
}
#demo {
    height: 200px;
}
@media (max-width: 480px){
#demo{
height: 220px;
}	
.sliderCol {
width: 50%;
text-align: center;
margin-bottom: 15px;
}
}
@media (max-width: 991px){
.sliderCol {
width: 33.33%;
text-align: center;
margin-bottom: 15px;
}
}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
	<li data-target="#demo" data-slide-to="2"></li>
	<li data-target="#demo" data-slide-to="3"></li>
	<li data-target="#demo" data-slide-to="4"></li>
	<li data-target="#demo" data-slide-to="5"></li>
	<li data-target="#demo" data-slide-to="6"></li>
	<li data-target="#demo" data-slide-to="7"></li>
	<li data-target="#demo" data-slide-to="8"></li>
  </ul>
  
  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active"><!--1st set-->
	<div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/ibm/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/IBM-Logo-255x255_165_165.png" alt="IBM" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/why-reporting-management-is-critical/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/NEC_logo-255x255_165_165.png" alt="NEC" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/why-digital-channel-marketing-benefits-traditional-media-buyers/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Matchnode-logo_165_165.png" alt="Matchnode" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/inventive-it/" target="_blank"><img src="https://owpdeve.wpengine.com/wp-content/uploads/2020/06/Inventive-IT-logo-web.png" alt="IHD" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/what-digital-transformation-looks-like-in-modern-laboratories/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/big-Solution4Labs_logo-255x255-1_165_165.png" alt="big-Solution4Labs" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/understanding-cloud-architectures-impact-in-the-world/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/big-CallCenterHosting_logo-255x255_165_165.png" alt="big-CallCenterHosting" width="165" height="165" class="img-fluid"></a></div>	  
	</div>
	</div><!--End of 1st set-->
    <div class="carousel-item"><!--2nd set-->
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/finance/ai-and-robotics-in-banks-and-financial-institutions/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/NorthernTrust-Logo-255x255_165_165.png" alt="NorthernTrust" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/overcoming-the-digital-age-disconnect/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/RingCentral-Logo-255x255_165_165.jpg" alt="RingCentral" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/datavail/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/thumb-datAvail-logo_165_165.jpg" alt="datAvail" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/email-marketing-trends-come-and-go-but-the-best-ones-stick-around/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/CRMIT-logo-255x255_165_165.jpg" alt="CRMIT" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/finance/why-business-continuity-for-finance-and-accounting-departments-matters/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Ace-Cloud-Hosting-Logo-255x255_165_165.jpg" alt="Ace-Cloud-Hosting" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/top-4-trending-advertising-formats/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Redtrack-logo-255x255_165_165.png" alt="Redtrack" width="165" height="165" class="img-fluid"></a></div>
	</div>
    </div><!--End of 2nd set-->
	<div class="carousel-item"><!--3rd set-->
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/pivotal-cloud-foundry-with-t-mobile/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/T-Mobile-logo-255x255_165_165.png" alt="T-mobile" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/pcloudy/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/pCloudy-Logo-255x255_165_165.png" alt="pCloudy" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/UNC_KenanFlagler_OWP-Logo-255x255_165_165.png" alt="UNC_KenanFlagler" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/key-trends-in-machine-learning-ai-and-cloud/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Zenoss_OWP-Logo-255x255_165_165.png" alt="Zenoss" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/dynamic-web-and-mobile-application-development-volume-5/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/GrapeCity_OWP-Logo-255x255_165_165.png" alt="GrapeCity" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/human-resources/hassle-free-best-practices-for-on-boarding/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Kronos_Logo_255x255_165_165.png" alt="Kronos" width="165" height="165" class="img-fluid"></a></div>
	</div>
    </div><!--End of 3rd set-->
	<div class="carousel-item"><!--4th set-->
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/go-to-meeting/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/LMI_GoToMeeting_V_Orange_HEX_255x255_165_165.png" alt="LMI_GoToMeeting_V_Orange_HEX" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/the-big-book-of-webinar-stats/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/LMI_GoToWebinar_V_Blue_HEX_255x255_165_165.png" alt="LMI_GoToWebinar_V_Blue_HEX" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/hcl/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/HCL_Technologies_logo-255x255_165_165.png" alt="HCL_Technologies" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/how-to-use-ai-ml-systems-to-revolutionize-testing-and-test-automation/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/TechMahendra_165_165.jpg" alt="TechMahendra" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/human-resources/voluntary-benefits-and-the-millennial-customer/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Trustmark_165_165.png" alt="Trustmark" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/developing-the-next-generation-of-metrology-software/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/TechSoft3d_165_165.png" alt="TechSoft3d" width="165" height="165" class="img-fluid"></a></div>
	</div>
    </div><!--End of 4th set-->
	
	<div class="carousel-item"><!--5th set-->
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/understanding-integrated-gsm-and-wifi-interception-system/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Shoghi_logo-255x255_165_165.png" alt="Shoghi" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/why-the-pci-dss-12-requirements-are-critical/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/security-metrics_165_165.png" alt="security-metrics" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/human-resources/why-paid-time-off-is-beneficial-to-employees-and-employers/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/ProjectManager__logo-255x255_165_165.png" alt="ProjectManager" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/human-resources/why-internal-communications-is-critical-for-employee-experience/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Poppulo-logo-255x255_165_165.png" alt="Poppulo" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/your-out-of-the-ordinary-guide-to-effective-content-optimization/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/MarketMuse__logo-255x255_165_165.png" alt="MarketMuse" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/finance/the-state-of-payment-processing-and-fraud-2018/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/kount_165_165.png" alt="kount" width="165" height="165" class="img-fluid"></a></div>
	</div>
    </div><!--End of 5th set-->
	
	<div class="carousel-item"><!--6th set-->
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/purchasing-strategy-methodology-plan/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Kepler__logo-255x255_165_165.png" alt="Kepler" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/finance/top-trends-impacting-account-takeover-fraud/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/experian_165_165.png" alt="experian" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/5-essential-elements-of-rugged-tablet-technology/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/estone_165_165.png" alt="estone" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/marketing/performing-cost-analysis-for-outsourcing-decisions/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/datamark_165_165.png" alt="datamark" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/why-mainframe-data-centers-need-modern-mobile-solutions/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/datakinetics_165_165.png" alt="datakinetics" width="165" height="165" class="img-fluid"></a></div>
	  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/cigniti/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Cigniti_logo-255x255_165_165.png" alt="Cigniti" width="165" height="165" class="img-fluid"></a></div>
	</div>
    </div><!--End of 6th set-->
	
	<div class="carousel-item"><!--7th set-->
    <div class="row">
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/erp-vs-plm-which-one-is-best/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/CentricSoftware__logo-255x255_165_165.png" alt="CentricSoftware" width="165" height="165" class="img-fluid"></a></div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/finance/banking-apis-and-microservices-at-enterprise-scale/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/Capco__logo-255x255_165_165.png" alt="Capco" width="165" height="165" class="img-fluid"></a></div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/delltechnologies.png" alt="" width="165" height="165" class="img-fluid"></a></div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/hewlett-packard.png" alt="" width="165" height="165" class="img-fluid"></a></div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/lightstep.png" alt="" width="165" height="165" class="img-fluid"></a></div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/microsoft.png" alt="" width="165" height="165" class="img-fluid"></a></div>	
	</div>
	</div><!--End of 7th set-->
	
	<div class="carousel-item">
			<!--8th set-->
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/rockwell.png"
							alt="rockwell" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/seeburger.png"
							alt="seeburger" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/softchoice.png"
							alt="softchoice" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/wells-fargo.png"
							alt="wells-fargo" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/zoominfo.png"
							alt="zoominfo" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="https://www.onlinewhitepapers.com/information-technology/embracing-zero-trust-network-access-ztna/"><img
							src="https://www.onlinewhitepapers.com/wp-content/uploads/2020/05/zscaler.png" alt="zscaler"
							width="165" height="165" class="img-fluid"></a></div>
			</div>
		</div>

		<div class="carousel-item">
			<!--9th set-->
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="https://www.onlinewhitepapers.com/information-technology/non-clinical-healthcare-programs-with-one-on-one-support-addresses-social-determinants/" target="_blank"><img src="https://www.onlinewhitepapers.com/wp-content/uploads/wp-responsive-images-thumbnail-slider/IHD-Logo_255x255_165_165.jpg"
							alt="rockwell" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://owpdeve.wpengine.com/wp-content/uploads/2019/10/blank.jpg"
							alt="seeburger" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://owpdeve.wpengine.com/wp-content/uploads/2019/10/blank.jpg"
							alt="softchoice" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://owpdeve.wpengine.com/wp-content/uploads/2019/10/blank.jpg"
							alt="wells-fargo" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a
						href="#" target="_blank"><img src="https://owpdeve.wpengine.com/wp-content/uploads/2019/10/blank.jpg"
							alt="zoominfo" width="165" height="165" class="img-fluid"></a></div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 sliderCol"><a href="#"><img
							src="https://owpdeve.wpengine.com/wp-content/uploads/2019/10/blank.jpg" alt="zscaler"
							width="165" height="165" class="img-fluid"></a></div>
			</div>
		</div>

    <!--<div class="carousel-item">
      <img src="ny.jpg" alt="New York" width="1100" height="500">
    </div>-->
  </div>
  
  <!-- Left and right controls -->
  <!--<a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>-->
</div>
<?php } ?>