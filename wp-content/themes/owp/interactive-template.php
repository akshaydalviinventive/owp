<?php 
/*
Template name: Interactive Page
Template Post Type: post
*/
get_header(); ?>
<style>
.single-post-box .post-thumbnail, .disclaimer-low, .right-thumb{display: none !important;}
.col-md-12{padding-right: 0;padding-left: 0;}
</style>

<div class="container-flex">
	<div class="row">
		<div class="col-md-12">
			<?php
			while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/single_content', get_post_format() );
			endwhile; // End of the loop.
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>