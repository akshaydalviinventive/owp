<?php 
/*
Template name: Landing page for Daniells
*/
get_header(); ?>
<style>
h3{font-size: 14px;}
</style>
	<div class="single-post-box">
		<div class="container">
			<div class="row">
			<?php
				while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/page_content', get_post_format() );
				endwhile; // End of the loop.
			?>
			</div>
		</div>	
	</div>
<?php get_footer(); ?>