<?php 
/*
Template Name: Home Page
*/
get_header();
?>
<?php get_template_part( 'home-page-header' ); ?>
<div class="container">
	<div class="row">
		<?php $args = array ( 'post_type' => 'post' ); query_posts( $args ); ?>
		<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/home_content', get_post_format() );
			endwhile;
			else :
				get_template_part( 'template-parts/home_content', 'none' );
			endif;
			if(function_exists('wp_paginate')):
			wp_paginate();  
			else :
			the_posts_pagination( array(
			'prev_text'          => __( 'Previous page', 'owp' ),
			'next_text'          => __( 'Next page', 'owp' ),
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'owp' ) . ' </span>',
			) );		
			wp_reset_query(); 
		?>
	</div>
</div>
<?php get_footer(); ?>