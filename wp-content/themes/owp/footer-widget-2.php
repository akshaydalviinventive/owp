<?php
if ( is_active_sidebar( 'footer-4' ) || is_active_sidebar( 'footer-5' ) ) {?>
        <div id="footer-widget-2">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'footer-4' )) : ?>
                        <div class="col-md-8"><?php dynamic_sidebar( 'footer-4' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-5' )) : ?>
                        <div class="col-md-4"><?php dynamic_sidebar( 'footer-5' ); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

<?php } ?>