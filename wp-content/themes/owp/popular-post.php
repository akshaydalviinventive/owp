<?php 
/*
Template name: Poplar Posts 
*/
get_header();

$args = array(
    'post_type' => 'post',
	'posts_per_page' => 10,
	'offset'=> 1,
	'orderby'=>'post_views_count', 
	'order'=>'DESC'
);
 
// Custom query.
$query = new WP_Query( $args );
?>
<header class="jumbotron text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="padding: 0px;">
					<?php
					if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
					else :
					the_title( '<h1 class="entry-title">', '</h1>' );
					endif;
					?>		
					<?php //echo do_shortcode( '[searchandfilter fields="search" submit_label="&#128269;"]' ); ?>
					<?php echo do_shortcode( '[searchandfilter id="5155"]' ); ?>
					<div class="home-cust-sort">
						<ul>
							<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li>
							<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>
							<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>-->
						</ul>
					</div>
				</div>
			</div>
		</div>
</header>

	<div class="container">

		<div class="row">
			<?php if ( $query->have_posts() ) : ?>

				<?php

				while ( $query->have_posts() ) : $query->the_post();

					get_template_part( 'template-parts/popular_content', get_post_format() );

				endwhile;
				
			else :

				get_template_part( 'template-parts/popular_content', 'none' );

			endif; 
			
			// Restore original post data.
			wp_reset_postdata();
			
			?>

		</div>

	</div>

<?php get_footer(); ?>