<?php 
/*
Template name: Manage Preferences
*/
get_header(); ?>
<style>
body { 
  background: url(https://www.onlinewhitepapers.com/wp-content/uploads/2018/09/home-bg-2.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
.header-part, .sb-wp, #footer-widget-1, #footer-widget-2{display: none;}
footer#colophon {background: #ffffffdb none repeat scroll 0 0;}
</style>
	<div class="single-post-box">
		<div class="container">
			<div class="row">
			<?php
				while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/page_content', get_post_format() );
				endwhile; // End of the loop.
			?>
			</div>
		</div>	
	</div>
<?php get_footer(); ?>