
<?php 
/*
Template Name: Blog Page
*/
get_header(); 

$blog = array ( 
		'post_type' => 'post',
		'order' => 'DESC' ,
		'cat' => '32'
);
$blog_args = new WP_Query($blog);
?>
<style>
.jumbotron {background-image: url(https://www.onlinewhitepapers.com/wp-content/uploads/2019/04/blog.jpg);}
.multi-cat{color: #fff;font-size: 36px;font-weight: 700;font-family: 'Montserrat', sans-serif;text-align: center;margin-top: 80px;}
</style>
<div class="jumbotron text-center" style="height: 163px;">
	<div class="container" style="margin-top: 54px;">
		<span class="multi-cat">Blog</a></span>
		<?php echo do_shortcode( '[searchandfilter fields="search" submit_label="&#128269;"]' ); ?>
		<!--<div class="home-cust-sort">
			<ul>
				<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li>
				<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>
				<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>
			</ul>
		</div>-->
	</div>
</div>

<div class="container"> 
	<div class="row">

		<?php if ( $blog_args->have_posts() ) : ?>
		<?php
		while ( $blog_args->have_posts() ) : $blog_args->the_post();
			get_template_part( 'template-parts/blog_page_archive_content', get_post_format() );
		endwhile;
		else :
			get_template_part( 'template-parts/blog_page_archive_content', 'none' );
		endif;

		//Pagination
		if(function_exists('wp_paginate')):
		wp_paginate();  
		else :
		the_posts_pagination( array(
		'prev_text'          => __( 'Previous page', 'owp' ),
		'next_text'          => __( 'Next page', 'owp' ),
		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'owp' ) . ' </span>',
		) );		
		endif;
		// Restore original post data.
		wp_reset_postdata();
		?>
	</div>
</div>			
<?php get_footer() ?>