<?php get_header(); ?>
<header class="jumbotron text-center">
	<div class="container">
		<h2><?php printf( esc_html__( 'Search Results for : %s', 'owp' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
		<?php //echo //do_shortcode( '[searchandfilter fields="search" submit_label="&#128269;"]' ); ?>
		<?php echo do_shortcode( '[searchandfilter id="5155"]' ); ?>
		<div class="home-cust-sort">
			<ul>
			<li><a href="https://www.onlinewhitepapers.com/popular/"><i class="fas fa-chart-line"></i> Popular</a></li>
			<li><a href="https://www.onlinewhitepapers.com/most-downloaded/"><i class="fas fa-download"></i> Most Downloaded</a></li>
			<!--<li><a href="#"><i class="fas fa-heart"></i> Favorites</a></li>-->
			</ul>
		</div>
	</div>	
</header>
<div class="container">
	<div class="row">
	<?php
	if ( have_posts() ) : ?>
	<?php 
	while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/search_content', 'search' );
	endwhile;
	else :
	?>
		
	<div class="col-md-12" style="margin: 10px 0px;"><h2 class="page-title"><?php printf( __( 'Sorry, no matches found', 'owp' )  ); ?></h2></div>
	<div class="col-md-12" style="margin: 10px auto;font-size: 14px;"><h4>Search Suggestions:</h4>
				<ul>
					<li>Check your spelling</li>
					<li>Try more general words</li>
					<li>Try different words that mean the same thing</li>
				</ul>
	</div>			
	<div class="col-md-12" style="margin: 10px 0px;"><h4>Or, perhaps these whitepapers might be of interest...</h4></div>			
	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 4,
			'offset'=> 1,
			'orderby'=>'post_views_count', 
			'order'=>'DESC'
		);
	$query = new WP_Query( $args );	
		if ( $query->have_posts() ) :
			while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'template-parts/popular_content', get_post_format() );
			endwhile;
		endif;
	endif;	
	wp_reset_postdata();
	?>
	</div>
</div>
<?php get_footer(); ?>
