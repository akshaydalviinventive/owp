<?php get_header(); ?>
<?php get_template_part( 'home-page-header' ); ?>
<div class="container">
	<div class="row">
		<?php
		$postnum = 0;		
		if ( have_posts() ) :
		while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/home_content', get_post_format() );	
		$postnum++;
		if ($postnum == 4){ ?>
		<!--Placement for banner AD
		<div class="col-md-12">
		<div class="row-banner-ad">
		</div>
		</div>-->
		<?php 
		}
		endwhile;
		else :
		?>
		<div class="col-md-12" style="margin: 10px 0px;">
			<h2 class="page-title"><?php printf( __( 'Sorry, no matches found', 'owp' )  ); ?></h2>
		</div>
		<div class="col-md-12" style="margin: 10px auto;font-size: 14px;"><h4>Search Suggestions:</h4>
			<ul>
				<li>Check your spelling</li>
				<li>Try more general words</li>
				<li>Try different words that mean the same thing</li>
			</ul>
		</div>			
		<div class="col-md-12" style="margin: 10px 0px;">
			<h4>Or, perhaps these whitepapers might be of interest...</h4>
		</div>			
		<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 4,
			'offset'=> 1,
			'orderby'=>'post_views_count', 
			'order'=>'DESC'
		);
		$query = new WP_Query( $args );	
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
					get_template_part( 'template-parts/popular_content', get_post_format() );
				endwhile;
			endif;
		endif;	
		wp_reset_postdata();
		?>
	</div>
</div>
<?php get_footer(); ?>