<?php function wp_subscriber_list() { ?>	

<style>
table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;margin-bottom: 30px;}
td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}
tr:nth-child(even){background-color: #dddddd;}
</style>

<!--Subscribers from wp users-->
<?php
global $wp_rewrite, $wp_query, $wpdb;
	$args1 = array(
	'role' => 'subscriber',
	'orderby' => 'ID',
	'order' => 'DESC'
	);
$subscribers = get_users($args1);
//echo $time = current_time( 'mysql' ). "<br>";
?>

<h1>Subscribers List Dashboard</h1>

<!--All day subscriber table-->
<h2>Subscriber List</h2>
	<table id="subscribers">
	<tr>
		<td colspan="4">Total Number of Registered Users</td>
		<td colspan="4">
		<?php
		// Get all users with role Subscribers.
		$user_query = new WP_User_Query( array( 'role' => 'subscriber' ) );
		// Get the total number of users for the current query. I use (int) only for sanitize.
		$users_count = (int) $user_query->get_total();
		// Echo a string and the value
		echo $users_count;
		?>
		</td>
	</tr>
	<tr>
		<!--<th>ID</th>-->
		<th>Name</th>
		<th>Mail ID</th>
		<th>Date and Time</th>
		<th>Website Url (source)</th>
		<th>user description</th>
	</tr>
	<?php foreach ($subscribers as $user) {?>
	<tr>
		<!--<td><?php //echo $user->ID;?></td>-->
		<td><?php echo $user->display_name;?></td>
		<td><?php echo $user->user_email;?></td>
		<td><?php echo $user->user_registered;?></td>
		<td><?php echo $user->user_url;?></td>
		<td><?php echo $user->description;?></td>
	</tr>
	<?php } ?>
	</table>
<!--End All day subscriber table-->

<!--Download history-->
<div class="cust-wp-list-panel">
<h1>Whitepapers download Dashboard</h1>
<h2>Whitepapers download List</h2>
    <table class="table">
        <thead>
        <tr>
            <th><?php _e('Package Name','download-manager'); ?></th>
            <th><?php _e('Download Time','download-manager'); ?></th>
            <th><?php _e('User/IP','download-manager'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $res = $wpdb->get_results("select p.ID as pid,p.post_title,s.* from {$wpdb->prefix}posts p, {$wpdb->prefix}ahm_download_stats s where s.pid = p.ID order by `timestamp` desc");
        foreach($res as $stat){
            ?>
            <tr>
                <td><?php echo $stat->post_title; ?></td>
                <td><?php echo date(get_option('date_format')." H:i",$stat->timestamp); ?></td>
                <td><?php echo $stat->uid > 0? get_user_by('id', $stat->uid)->display_name . " / ":''; ?><?php echo $stat->ip; ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
<!--code end-->

<!--All combine data-->
<h1>All combine data</h1>
<table class="table">
<thead>
<tr>
<th><?php _e('Package Name','download-manager'); ?></th>
<th><?php _e('Download Time','download-manager'); ?></th>
<th><?php _e('User/IP','download-manager'); ?></th>
<th>Name</th>
<th>Mail ID</th>
<th>Date and Time</th>
<th>Website Url (source)</th>
</tr>
</thead>
<tbody>
<?php $res_f = $wpdb->get_results("select p.ID as pid,p.post_title,s.*,u.* from {$wpdb->prefix}posts p, {$wpdb->prefix}ahm_download_stats s,{$wpdb->prefix}users u where s.pid = p.ID and u.ID=s.uid order by `timestamp` desc");
        foreach($res_f as $stat_f){ ?>
            <tr>
                <td><?php echo $stat_f->post_title; ?></td>
                <td><?php echo date(get_option('date_format')." H:i",$stat_f->timestamp); ?></td>
                <td><?php echo $stat_f->uid > 0? get_user_by('id', $stat_f->uid)->display_name . " / ":''; ?><?php echo $stat_f->ip; ?></td>
				<td><?php echo $stat_f->display_name;?></td>
				<td><?php echo $stat_f->user_email;?></td>
				<td><?php echo $stat_f->user_registered;?></td>
				<td><?php echo $stat_f->user_url;?></td>
            </tr>
  <?php } ?>
</tbody>
</table>
<!--code end-->
<?php } ?>