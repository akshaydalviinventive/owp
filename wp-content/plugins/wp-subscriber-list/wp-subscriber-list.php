<?php
/*
Plugin Name: Wordpress subscriber database list
Plugin URI: 
Description: Display all subscribers from wordpress users
Version: 1.0
Author: Bhushan R Shirapure
Author URI: 
*/

defined ( 'ABSPATH' ) or die ('404');

	// function to Start plugin / Options / Defaults
	function start_plugin() {}
	register_activation_hook( __FILE__, 'start_plugin' );

    //menu items	
	add_action('admin_menu','wp_subscriber_list_menu');
	function wp_subscriber_list_menu() {
		//this is the main item for the menu
		add_menu_page('wp subscriber list dashboard', //page title
		'wp subscriber list dashboard option', //menu title
		'manage_options', //capabilities
		'wp_subscriber_list', //menu slug
		'wp_subscriber_list', //callback function
		'dashicons-welcome-widgets-menus', //dash icon
		'81' //Position
	);}	
	define('ROOTDIR', plugin_dir_path(__FILE__));
	require_once(ROOTDIR . 'wpdb-list.php');
?>