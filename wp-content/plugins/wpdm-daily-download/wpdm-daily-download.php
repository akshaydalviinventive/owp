<?php
/*
Plugin Name: WPDM - Download Limits
Description: Enable WPDM Pro to set download limit / * day(s) for single user and user roles
Plugin URI: http://www.wpdownloadmanager.com/
Author: Shaon
Version: 2.4.1
Author URI: http://www.wpdownloadmanager.com/
*/

global $download_limit;


function wpdm_dd_check_user_download($package){
    global $current_user;
    if(\WPDM\Package::validateMasterKey($package['ID'], wpdm_query_var('masterkey'))) return $package;

    if(class_exists('\WPDMPP\WPDMPremiumPackage') && \WPDMPP\WPDMPremiumPackage::wpdmpp_is_purchased($package['ID'], get_current_user_id())) return $package;

    if(is_user_logged_in()){

        if((int)get_current_user_id() === (int)$package['post_author']) return $package;

        $time = get_user_meta($current_user->ID,'wpdm_reset_time',true);
        if($time < time()) {
            update_user_meta($current_user->ID,'wpdm_reset_time',strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
            update_user_meta($current_user->ID,'wpdm_dd_user_dlc',0);
        }
    } else {
        $time = get_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),true);
        if($time<time()) {
            update_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
            $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
            update_option($gkey,0);
        }
    }

    //s
    if(!is_user_logged_in()){
        $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
        $user_download_count = (int)get_option($gkey,0);
    } else
        $user_download_count = (int)get_user_meta($current_user->ID,'wpdm_dd_user_dlc',true);

    $global_download_limit =  0;
    $global_role_ddl = get_option('_wpdm_global_role_limit',array());

    $roles = is_user_logged_in()?$current_user->roles :array('guest');
    $role_download_limit = 0;

    foreach ($roles as $role) {
        $role_download_limit = $role_download_limit + (int)$global_role_ddl[$role];
    }

    $user_download_limit = $role_download_limit?$role_download_limit:$global_download_limit;

    //update_user_meta($current_user->ID,'wpdm_dd_user_dlc',0);
    if($user_download_count>$user_download_limit && $user_download_limit>0) {
        $message= "Thanks for staying with us. Unfortunately your daily download limit ( $user_download_count ) already exceeded for this package. Please try again tomorrow.";
        do_action("wpdm_user_download_limit_expired", $package);
        header("Content-Description: File Transfer");
        header('Content-type: text/plain');
        header("Content-Disposition: attachment; filename=\"daily-download-limit-exceeded.txt\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . strlen($message));
        die($message);
    }  
     
    return $package;
}

function wpdm_dd_update_user_download($p){
    global $current_user;
    if(\WPDM\Package::validateMasterKey($p['ID'], wpdm_query_var('masterkey'))) return;

    if(is_user_logged_in()){
        //if((int)get_current_user_id() === (int)$p['post_author']) return;
        $user_download_count = (int)get_user_meta($current_user->ID,'wpdm_dd_user_dlc',true);
        $user_download_count++;
        update_user_meta($current_user->ID,'wpdm_dd_user_dlc',$user_download_count);
    } else {     
        $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
        $user_download_count = (int)get_option($gkey,0);
        $user_download_count++;
    update_option($gkey,$user_download_count);
    }

}

function wpdm_dd_download_link($package){
    global $current_user;

    if(is_user_logged_in()){

        if((int)get_current_user_id() === (int)$package['post_author']) return $package;

        $time = get_user_meta($current_user->ID,'wpdm_reset_time',true);

        if($time<time()) {
            update_user_meta($current_user->ID,'wpdm_reset_time',strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
            update_user_meta($current_user->ID,'wpdm_dd_user_dlc',0);
        }} else {

        $time = get_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),true);
        if($time<time()) {
            update_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
            $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
            update_option($gkey,0);
        }
    }


    if(!is_user_logged_in()){
        $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
        $user_download_count = (int)get_option($gkey,0);
    } else
        $user_download_count = (int)get_user_meta($current_user->ID,'wpdm_dd_user_dlc',true);
//echo $user_download_count; die();
    $global_download_limit =  0;
    $global_role_ddl = get_option('_wpdm_global_role_limit',array());

    //$role = is_user_logged_in()?$current_user->roles[0]:'guest';
    //$role_download_limit = $global_role_ddl[$role];
    if(is_user_logged_in()){
        $role_download_limit = 0;
        foreach($current_user->roles as $role){
            $role_download_limit += isset($global_role_ddl[$role])?(int)$global_role_ddl[$role]:0;
        }
    } else{
        $role_download_limit = (int)$global_role_ddl['guest'];
    }

    $user_download_limit = $role_download_limit?$role_download_limit:$global_download_limit;

    if($user_download_count>$user_download_limit && $user_download_limit>0) {
        $limit_msg = get_option("__wpdm_download_limit_exceeded");
        $limit_msg = trim($limit_msg)?$limit_msg:__( "Download limit exceeded!" , "download-manager" );
        $package['download_link'] = $limit_msg;
    }
    return $package;
}

function wpdm_dd_limit_by_roles(){
    global $post;
     
    ?>
    

<?php
    $role_download_limit = get_post_meta($post->ID,'__wpdm_role_limit', true);
 
    
    ?>

<table class="table widefat fixed frm">
    <thead>
<tr><th style="width:200px">User Role</th><th>Download Limit</th></tr>
    </thead>
    <tr><td>Guests</td><td><input type="text" name="file[role_limit][guest]" value="<?php if(isset($role_download_limit['guest'])) echo $role_download_limit['guest']; ?>" /></td></tr>
    <?php
    global $wp_roles;
    $roles = array_reverse($wp_roles->role_names);    
    foreach( $roles as $role => $name ) { 
    ?>
    <tr><td><?php echo $name; ?></td><td><input type="text" name="file[role_limit][<?php echo $role; ?>]" value="<?php if(isset($role_download_limit[$role])) echo $role_download_limit[$role]; ?>"></td></tr>
    <?php } ?>
    </table>

        <div class="clear"></div>


    
    <?php
}

function wpdm_ddl_tab($tabs){
    $tabs['wpdm_dd_limit_by_roles'] = array('name' => __('Download Limit', "wpdmpro"), 'callback' => 'wpdm_dd_limit_by_roles');
    return $tabs;
}

function wpdm_dd_global_limit_by_roles(){
    global $current_user;
     
    ?>

 
<?php
    $global_role_ddl = get_option('_wpdm_global_role_limit',array());
 
    
    ?>
         <div class="panel panel-default">
             <div class="panel-heading">Download Limit</div>
         <table class="table">
             <tr><td>Limit Reset Period:</td><td style="width: 150px;text-align: right">
                     <div class="input-group">
                         <input type="number" name="_wpdm_ddl_period" class="form-control input-sm" value="<?php echo get_option('_wpdm_ddl_period',1); ?>" />
                         <div class="input-group-addon"> day(s) </div>
                     </div>

                 </td></tr>

             <tr><th colspan="2">Limit Download By User Roles</th> </tr>
<tr><th>User Role</th><th>Download Limit</th></tr>
    <tr><td>Guests</td><td style="width: 100px;text-align: right"><input  class="form-control input-sm" style="width: 100px" type="text" name="_wpdm_global_role_limit[guest]" value="<?php echo isset($global_role_ddl['guest'])?$global_role_ddl['guest']:""; ?>" /></td></tr>
    <?php
    global $wp_roles;
    $roles = array_reverse($wp_roles->role_names);    
    foreach( $roles as $role => $name ) { 
    ?>
    <tr><td><?php echo $name; ?></td><td style="width: 100px;text-align: right"><input  class="form-control input-sm" style="width: 100px" type="text" size="5" name="_wpdm_global_role_limit[<?php echo $role; ?>]" value="<?php echo isset($global_role_ddl[$role])?$global_role_ddl[$role]:""; ?>"></td></tr>
    <?php } ?>
    </table>

         </div>

 

    
    <?php
}

class DownloadLimit{
    function __construct()
    {
        add_filter("wpdm_user_download_limit_exceeded", array($this, 'checkLimit'),10, 2);
        add_shortcode("wpdm_user_download_limit", array($this, 'downloadLimit'));
        add_shortcode("wpdm_user_download_count", array($this, 'downloadCount'));
        add_shortcode("wpdm_download_limit_reset_timer", array($this, 'downloadLimitResetTimer'));
    }

    function checkLimit($limitExeeded, $packageID)
    {
        global $current_user;

        $package = get_post($packageID);

        if(is_user_logged_in()){

            //if((int)get_current_user_id() === (int)$package->post_author) return $limitExeeded;

            $time = get_user_meta($current_user->ID,'wpdm_reset_time',true);

            if($time<time()) {
                update_user_meta($current_user->ID,'wpdm_reset_time',strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
                update_user_meta($current_user->ID,'wpdm_dd_user_dlc',0);
            }} else {

            $time = get_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),true);
            if($time<time()) {
                update_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),strtotime("+".get_option("_wpdm_ddl_period",1)." day"));
                $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
                update_option($gkey,0);
            }
        }


        if(!is_user_logged_in()){
            $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
            $user_download_count = (int)get_option($gkey,0);
        } else
            $user_download_count = (int)get_user_meta($current_user->ID,'wpdm_dd_user_dlc',true);
        $global_download_limit =  0;
        $global_role_ddl = get_option('_wpdm_global_role_limit',array());

        if(is_user_logged_in()){
            $role_download_limit = 0;
            foreach($current_user->roles as $role){
                $role_download_limit += isset($global_role_ddl[$role])?(int)$global_role_ddl[$role]:0;
            }
        } else{
            $role_download_limit = (int)$global_role_ddl['guest'];
        }

        $user_download_limit = $role_download_limit ? $role_download_limit : $global_download_limit;

        if($user_download_count > $user_download_limit && $user_download_limit > 0) {
            $limitExeeded = true;
        }
        return $limitExeeded;
    }

    function downloadLimit($params = array()){
        global $current_user;
        $user = isset($params['user'])?(int)$params['user']:get_current_user_id();
        $user = (int)$user === (int)$current_user->ID ? $current_user : get_user_by('id', $user);

        $global_download_limit =  0;
        $global_role_ddl = get_option('_wpdm_global_role_limit',array());

        if(is_user_logged_in()){
            $role_download_limit = 0;
            foreach($user->roles as $role){
                $role_download_limit += isset($global_role_ddl[$role])?(int)$global_role_ddl[$role]:0;
            }
        } else{
            $role_download_limit = (int)$global_role_ddl['guest'];
        }

        $user_download_limit = $role_download_limit ? $role_download_limit : $global_download_limit;
        if(isset($params['remains']) && (int)$params['remains'] === 1) {
            if(!is_user_logged_in()){
                $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
                $user_download_count = (int)get_option($gkey,0);
            } else
                $user_download_count = (int)get_user_meta($current_user->ID,'wpdm_dd_user_dlc',true);
            return (int)$user_download_limit - (int)$user_download_count;
        }
        return $user_download_limit;

    }

    function downloadCount($params = array()){
        global $current_user;
        $user = isset($params['user'])?(int)$params['user']:get_current_user_id();
        if(!is_user_logged_in()){
            $gkey = 'wpdm_dd_user_dlc_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']);
            $user_download_count = (int)get_option($gkey,0);
        } else
            $user_download_count = (int)get_user_meta($user,'wpdm_dd_user_dlc',true);
        return $user_download_count;

    }

    function downloadLimitResetTimer(){
        global $current_user;
        if(is_user_logged_in())
            $time = get_user_meta(get_current_user_id(), 'wpdm_reset_time', true);
        else
            $time = get_option('wpdm_reset_time_'.str_replace(":","_",$_SERVER['REMOTE_ADDR']),true);
        ob_start();
        ?>
        <span id="wpdm-download-limit-reset-timer" style="display: inline-block">

            <span id="wpdm-dl-timer"></span>

        <script>
            /* Set the date we're counting down to */
            var countDownDate = <?php echo $time * 1000; ?>;

            /* Update the count down every 1 second */
            var x = setInterval(function () {


                var now = new Date().getTime();

                var distance = countDownDate - now;

                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                if ((days + "").length < 2) days = "0" + days;
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                if ((hours + "").length < 2) hours = "0" + hours;
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                if ((minutes + "").length < 2) minutes = "0" + minutes;
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                if ((seconds + "").length < 2) seconds = "0" + seconds;

                document.getElementById("wpdm-dl-timer").innerHTML = "<span class='label label-warning'>" + days + " days</span> <span class='label label-primary'>" + hours + " hours</span> <span class='label label-info'>"
                    + minutes + " minutes</span> <span class='label label-success'>" + seconds + " seconds</span>";

                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("wpdm-dl-timer").innerHTML = "&mdash;";
                }
            }, 1000);
        </script>
        </span>
        <?php
        return ob_get_clean();
    }


}
if(defined('WPDM_Version'))
$download_limit =  new DownloadLimit();

add_filter('wpdm_after_prepare_package_data','wpdm_dd_download_link');
add_filter('wdm_before_fetch_template','wpdm_dd_download_link');
add_filter('before_download','wpdm_dd_check_user_download');
add_action('wpdm_onstart_download','wpdm_dd_update_user_download');

add_action('basic_settings_section','wpdm_dd_global_limit_by_roles');


