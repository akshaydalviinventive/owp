<?php
/*
Plugin Name: Tags per post
Plugin URI: 
Description: Display tags as per individual post.
Version: 1.0
Author: Bhushan R Shirapure
Author URI: 
*/
defined ( 'ABSPATH' ) or die ('404');
function display_Tags(){
	ob_start();
	the_tags( '<ul><li>', '</li><li>', '</li></ul>' );
	$output = ob_get_clean();    
	return $output;
}
add_shortcode('tpg', 'display_Tags');
?>